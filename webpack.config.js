/*
    ./webpack.config.js
*/
const path = require('path');
module.exports = {
  entry: './index.js',
  output: {
  //  path: path.resolve('./'),
    filename: 'bundle.js'
  },
  watch:true,
  module: {
    loaders: [
      { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
      { test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/ }
    ]
  },
  resolve: {
    extensions: ['*', '.js', '.jsx'],
  }
}