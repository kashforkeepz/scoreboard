import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import Header from './components/head.jsx';
import Player from './components/player.jsx';
import  { players } from './constants.js';
import Stats from './components/stats.jsx';
import axios from 'axios';

export default class App extends Component {
	constructor(){
		super();
		this.state = {
			players:[],
			loading: true
		}
	}
	componentWillMount(){

		const that = this;

		axios.get('https://teamtreehouse.com/jonathanschneider.json')
		.then(function(response) {
			console.log(response);

		that.setState({players: response.data.badges, loading: false});
		})
	}

	render() {
		return(
	<div className="scoreboard">
		{!this.state.loading &&
		<div>
		<Header title={this.props.title} />
			<div className="players">
				{this.state.players.map(function(player) {
					return <Player name={player.name} key={player.id} /> 
				})}
			</div>
		</div> 
		}
		{this.state.loading &&
			<div>
			<h1>loading</h1>
			</div>
		}			
	</div>
			);
	}
}

/*PropTypes.propTypes: {
		title: PropTypes.string,
		players: PropTypes.arrayOf(React.PropTypes.shape({
			name: PropTypes.string.isRequired,
			score: PropTypes.number.isRequired,
			id: PropTypes.number.isRequired,
		})).isRequired,
	},*/

ReactDOM.render(<App />, document.getElementById('container'));