import React, { Component } from 'react';
import Counter from './counter.jsx';

function Player( {name , score } ){
		return(
				<div className="player">
		          <div className="player-name">
		          {name}
		          </div>
		          <div className="player-score">
		            <Counter score={score} />            
		          </div>
    			</div>
			);
	}
export default Player;
