import React, { Component } from 'react';
import Counter from './counter.jsx';
import Header from './head.jsx';
import Plays from '../constants.js';
import PropTypes from 'prop-types';

class Stats extends Component {
//var totalPlayers = this.Plays.length;

	render() {

		Plays.players.propTypes = {
	onScoreChange: React.PropTypes.func.isRequired,
}

	var totalPoints = Plays.players.reduce(function(total, player) {
		return total + Plays.players.score;
	}, 0);

	function onScoreChange(delta) {
		console.log('onScoreChange', delta);
	}
		return (
			
			<table className="stats">
				<tbody>
					<tr>
						<td>Players:</td>
						<td>{Plays.players.length}</td>
					</tr>
					<tr>
						<td>Total Points:</td>
						<td>123</td>
					</tr>

				</tbody>
			</table>
				)
	}
}

export default Stats;