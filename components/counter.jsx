import React, { Component } from 'react';

/*class Counter extends Component {

	constructor() {
		super()
}

	componentWillMount() {
		this.state = {
			score: 0
		}
	}

	decrement(score) {
		this.setState({
			score: this.state.score - 1
		});
	}

	handleClick(score) {
		this.setState({
			score: this.state.score + 1
		});
	}

	//<button className="counter-action increment" onClick={() => this.handleClick()}> + </button>
	
render() {
	}
}*/

	function Counter(props) {		

	/*	Counter.propTypes = {
			score: PropTypes.number.isRequired,
			onChange: PropTypes.func.isRequired,
		}*/


		return (
			
			<div className="counter">
		      {<button className="counter-action decrement" onClick={function() {props.onChange(-1);}} > - </button>}
		      <div className="counter-score"> {/*props.score*/} </div>
		      {<button className="counter-action increment" onClick={function() {props.onChange(1);}}> + </button>}
		 	</div>

			 )
	}

export default Counter;