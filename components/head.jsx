import React, { Component } from 'react';
import Stats from './stats.jsx';

function Header( {title} ){
	  return (
              <div className="header">
              <Stats />
              <h1>{title}</h1>
              </div>
        );
}
export default Header;